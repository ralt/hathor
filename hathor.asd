(asdf:defsystem #:hathor
  :description "Persistent key-value database."
  :author "Florian Margaine <florian@margaine.com>"
  :license "GPLv3"
  :depends-on (:usocket-server
               :bordeaux-threads
               :cl-syslog
               :babel
               :flexi-streams)
  :serial t
  :components ((:module "src"
                        :components ((:file "package")
                                     (:file "helper" :depends-on ("package"))
                                     (:file "condition" :depends-on ("package"))
                                     (:file "server" :depends-on ("package"
                                                                  "helper"
                                                                  "ping"
                                                                  "set"))
                                     (:file "lock" :depends-on ("package"))
                                     (:file "code" :depends-on ("package"))
                                     (:file "cache" :depends-on ("package"
                                                                 "code"))
                                     (:file "storage" :depends-on ("package"
                                                                   "lock"
                                                                   "cache"
                                                                   "helper"))
                                     (:file "set" :depends-on ("package"
                                                               "helper"))
                                     (:file "get" :depends-on ("package"
                                                               "helper"))
                                     (:file "ping" :depends-on ("package"
                                                                "helper"))
                                     (:file "hathor")))))
