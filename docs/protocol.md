# protocol

The client needs to send messages in a specific protocol for the
server to understand them. The server will reply in a specific format
for the client to understand it.

This is the protocol.

The protocol is a simple request-response protocol.

In this document, 1 byte = 8 bits.

In general, network byte order (big endian) is used.

## request

The client needs to send messages in the following format:

- First byte is the command to execute

This is the list of values allowed:

- 0: PING
- 1: SET
- 2: GET
- 3: DELETE
- 4: ACQUIRE_LOCK
- 5: RELEASE_LOCK

If the value is not recognized, the server closes the socket.

For each command, the following bytes differ.

### PING

For the PING command, further bytes are ignored and the server replies
with a PONG.

### SET

For the SET command, the following format is needed:

- The first 4 bytes are read. They define the timestamp of the
  expiration date, in seconds.

- The next 2 bytes are used to define the length of the key to
  set. This gives a maximum of 65535 bytes for the key length. These
  bytes give you the number x.

- The next x bytes are read. They define the key to set.

- The next 4 bytes are used to define the length of the value to set
  for the key. This gives a maximum of 4 billion bytes (4GB) for the
  value length. These bytes give you the number y.

- The next y bytes are read. They define the value to set.

The server then replies with a response indicating success or error,
and more information in case of an error.

### GET

For the GET command, the following format is needed:

- The first 2 bytes are used to define the length of the key to
  get. These bytes give you the number x.

- The next x bytes are read. They define the key to get.

The server replies with a response including the value or an error,
and more information in case of an error.

### DELETE

For the DELETE command, the following format is needed:

- The first 2 bytes are used to define the length of the key to
  delete. These bytes give you the number x.

- The next x bytes are read. They define the key to delete.

The server then replies with a response indicating success or error,
and more information in case of an error.

### ACQUIRE_LOCK

TODO

### RELEASE_LOCK

TODO

## response

The server will follow this format for the responses of these requests:

### PING

The server replies with one byte. The value of the byte can be
ignored.

### SET

The server replies with the following format:

- The first byte defines whether the SET has succeeded or not.

  - If the value is 0, the SET has succeeded.
  - If the value is >= 1 and < 255, the SET failed, and this value
    defines the error code.
  - If the value is 255, the SET failed, and the client needs to read
    the next byte to know the error code.

- In case of an error, the next 2 bytes define the length of the error
  message. These bytes give you the number x.

- The next x bytes are read by the client to get the error message.

### GET

The server replies with the following format:

- The first byte defines whether the GET has succeeded or not.

  - If the value is 0, the GET has succeeded.
  - If the value is >= 1 and < 255, the SET failed, and this value
    defines the error code.
  - If the value is 255, the SET failed, and the client needs to read
    the next byte to know the error code.

- In case of an error, the next 2 bytes define the length of the error
  message. These bytes give you the number x.

- The next x bytes are read by the client to get the error message.

- If the GET was successful, the next 4 bytes define the length of the
  returned value. These bytes give you the number y.

- The next y bytes are read by the client to get the key value.

### DELETE

The server replies with the following format:

- The first byte defines whether the DELETE has succeeded or not.

  - If the value is 0, the DELETE has succeeded.
  - If the value is >= 1 and < 255, the DELETE failed, and this value
    defines the error code.
  - If the value is 255, the DELETE failed, and the client needs to
    read the next byte to know the error code.

- In case of an error, the next 2 bytes define the length of the error
  message. These bytes give you the number x.

- The next x bytes are read by the client to get the error message.

### ACQUIRE_LOCK

TODO

### RELEASE_LOCK

TODO
