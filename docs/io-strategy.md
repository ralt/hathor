# IO strategy

Or how the IO is distributed between threads.

The storage object lives in a separate thread later named "storage
thread".

The server thread listens for incoming connections.

Every client gets a new thread (later named "client thread"), and the
thread remains open for multiple requests/responses as long as the
CLOSE request isn't sent, or the client closes the socket.

Whenever a client thread wants to write, it sends the write to the
storage thread and waits for a confirmation.

Whenever a client thread wants to read, it sends the read to the
storage thread and waits for the value. If there's no returned value,
it takes a read-only lock on the latest snapshot file value, reads the
file and tries to find the value.

Whenever the storage thread needs to write, it writes both to the
in-memory storage and appends to the log file. It also sends to the
expiration management thread the new values. Since it's within the
same thread, there's no need for any locking.

Whenever the storage threads needs to read, it just looks in its
in-memory storage. This way of letting each client thread read the
latest snapshot file lets us parallelize the searches in the file.

The separate thread that creates new snapshot files takes a read-write
lock on the latest snapshot file value and replaces it after it's
written. This lets every client thread share a read-only lock and wait
for the read-write lock when the latest snapshot file value is being
replaced.

The expiration management thread gets new values from the storage
thread, and sends deletions to the storage thread whenever a key
expires.

Finally, the main thread is an REPL.
