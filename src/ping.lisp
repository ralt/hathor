(in-package #:hathor)

(defun command-ping (storage stream)
  "PONG says the fox"
  (declare (ignore storage))
  (write-byte 0 stream))
