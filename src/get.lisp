(in-package #:hathor)

(defun command-get (storage stream)
  "Gets a value for a key."
  (let ((key (read-byte-stream stream (bytes->integer
                                       (read-byte-stream stream 2)))))
    (handler-case
        (write-sequence (get-value storage key) stream)
      (read-error (err)
        (write-error-to-stream err stream)))))
