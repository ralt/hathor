(in-package #:hathor)

(defclass limited-sorted-hash-table ()
  ((capacity :initarg :capacity :reader table-capacity)
   (table :accessor table)))

(defmethod initialize-instance ((table limited-sorted-hash-table) &key)
  (setf (table table) (make-hash-table :test #'equal
                                       :size (table-capacity table))))

(defun cache-get-value (table key)
  (multiple-value-bind (value presentp)
      (gethash key (table table))
    (declare (ignore presentp))
    value))

(defun cache-set-value (table key value)
  (when (and (>= (hash-table-count (table table)) (table-capacity table))
             (multiple-value-bind (value presentp)
                 (gethash key (table table))
               (declare (ignore value))
               (not presentp)))
    (error 'write-error
           :message "Cache capacity exceeded."
           :code *cache-capacity-exceeded*))
  (setf (gethash key (table table)) value))

(defun cache-delete-key (table key)
  (remhash key (table table)))
