(in-package #:hathor)

(defun command-set (storage stream)
  "Sets a value for a key."
  (let ((expiration-timestamp (bytes->integer
                               (read-byte-stream stream 4)))
        (key (read-byte-stream stream (bytes->integer
                                       (read-byte-stream stream 2))))
        (value (read-byte-stream stream (bytes->integer
                                         (read-byte-stream stream 4)))))
    (handler-case
        (progn
          (set-value storage key value expiration-timestamp)
          (write-byte 0 stream))
      (write-error (err)
        (write-error-to-stream err stream)))))
