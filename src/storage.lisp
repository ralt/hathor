(in-package #:hathor)

(defclass item ()
  ((key :initarg :key :reader key)
   (value :initarg :value :reader value)
   (expiration-time :initarg :expiration-time :reader expiration-time)))

(defun serialize (item)
  (concatenate '(vector octet)
               (integer->bytes (expiration-time item) 4)
               (integer->bytes (length (key item)) 2)
               (babel:string-to-octets (key item))
               (integer->bytes (length (value item)) 4)
               (babel:string-to-octets (value item))))

(defun deserialize (bytes)
  (let* ((byte-stream (flexi-streams:make-in-memory-input-stream bytes))
         (expiration-timestamp (bytes->integer
                                (read-byte-stream byte-stream 4)))
         (key (read-byte-stream stream (bytes->integer
                                        (read-byte-stream byte-stream 2))))
         (value (read-byte-stream stream (bytes->integer
                                          (read-byte-stream byte-stream 4)))))
    (make-instance 'item
                   :key key
                   :value value
                   :expiration-time expiration-time)))

(defclass storage ()
  ((stream :accessor storage-stream)
   (lock :accessor lock :type seqlock)
   (cache :accessor cache :type limited-sorted-hash-table)))

(defmethod initialize-instance ((storage storage) &key)
  (setf (lock storage) (make-instance 'seqlock))
  (setf (cache storage) (make-instance 'limited-sorted-hash-table
                                       ;; TODO: make this configurable.
                                       ;; duh.
                                       :capacity 50)))

(defun start-storage (storage callback)
  (setf (storage-stream storage)
        (open "log"
              :direction :output
              :if-does-not-exist :create
              :if-exists :append)))

(defun stop-storage (storage)
  (close (storage-stream storage)))

(defun set-value (storage key value expiration-time)
  (let ((item (make-instance 'item
                             :key key
                             :value value
                             :expiration-time expiration-time)))
    (with-writer-seqlock ((lock storage))
      (cache-set-value (cache storage) key (serialize item))
      ;; Define log format.
      (write-sequence #(0) (storage-stream storage)))))

(defun get-value (storage key)
  (with-reader-seqlock ((lock storage))
    (let ((serialized-item (cache-get-value (cache storage) key)))
      (if serialized-item
          (deserialize serialized-item)
          ;; Is it in the snapshot file?
          ))))

(defun delete-key (storage key))
