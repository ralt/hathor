(in-package #:hathor)

(defclass seqlock ()
  ((mutex :accessor mutex)
   (counter :accessor counter :initform 0)))

(defmethod initialize-instance ((lock seqlock) &key)
  (setf (mutex lock) (bt:make-lock)))

(defmacro with-reader-seqlock (var &body body)
  (let ((lock (first var)))
    `(block lock-loop
       (loop
          (let ((original-counter (counter ,lock))
                (result))
            (setf result (progn ,@body))
            (when (and (evenp original-counter)
                       (= original-counter (counter ,lock)))
              (return-from lock-loop result)))))))

(defmacro with-writer-seqlock (var &body body)
  (let ((lock (first var)))
    `(progn
       (bt:acquire-lock (mutex ,lock))
       (incf (counter ,lock))
       (unwind-protect
            (progn ,@body)
         (progn
           (incf (counter ,lock))
           (bt:release-lock (mutex ,lock)))))))
