(in-package #:hathor)

(deftype octet () '(unsigned-byte 8))
(deftype octet-vector () '(simple-array octet (*)))

(defun read-byte-stream (stream length)
  (let ((buf (make-array length :element-type 'octet)))
    (read-sequence buf stream)
    buf))

(defun bytes (vector)
  (coerce vector '(vector octet)))

(defun syslog (&rest args)
  (apply #'syslog:log "hathor" :local7 args))

(defun integer->bytes (integer bytes-length)
  (let ((buf (make-array bytes-length
                         :element-type 'octet
                         :initial-element 0)))
    (loop for i from (1- bytes-length) downto 0
       do (setf (elt buf (- (1- bytes-length) i))
                (ldb (byte 8 (* 8 i)) integer)))
    buf))

(defun bytes->integer (bytes)
  (reduce #'+
          (let ((b (reverse bytes)))
           (loop for i from 0 upto (1- (length b))
              collect (ash (elt b i) (* 8 i))))))

(defun write-error-to-stream (err stream)
  (write-sequence
   (concatenate 'vector
                (make-array 1 :initial-element (code err))
                (integer->bytes (length (message err)) 2)
                (map 'vector #'char-code (message err)))
   stream))
