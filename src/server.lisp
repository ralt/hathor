(in-package #:hathor)

(defclass server ()
  ((socket :type usocket:socket :accessor socket)
   (address :reader address :initarg :address)
   (port :reader port :initarg :port)
   (stopped :type bool :accessor stopped :initform nil)
   (thread :accessor thread)))

(defun accept-loop (server callback arguments)
  (unwind-protect
       (loop
          (let ((client-socket (usocket:socket-accept
                                (socket server)
                                :element-type 'octet)))
            (bt:make-thread
             (lambda ()
               (accept-client client-socket callback arguments))
             :name "hathor client"))
          (when (stopped server)
            (return)))
    (usocket:socket-close (socket server))))

(defun accept-client (socket callback arguments)
  (let ((socket-stream (usocket:socket-stream socket)))
    (unwind-protect
         (apply callback
                socket-stream
                arguments)
      (close socket-stream)
      (usocket:socket-close socket))))

(defun start-server (address port storage commands)
  ;; usocket:socket-server is nice if you don't ever
  ;; want to stop it. There's no clean way to stop it.
  ;; We're basically going to reproduce what it is doing,
  ;; while adding our custom sauce to make it stoppable.
  (let ((server (make-instance 'server :address address :port port)))
    (setf (socket server) (usocket:socket-listen address port
                                                 :reuse-address t))
    (setf (thread server)
          (bt:make-thread
           (lambda ()
             (accept-loop
              server
              #'message-handler
              (list commands (length commands) storage)))
           :name "hathor server"))
    server))

(defun stop-server (server)
  (setf (stopped server) t)
  ;; Send a simple PING to wake up the accept loop
  (usocket:with-client-socket (socket
                               stream
                               (address server)
                               (port server)
                               :element-type 'octet)
    (write-byte 0 stream)
    (force-output stream))
  (bt:join-thread (thread server)))

(defun message-handler (stream commands commands-length storage)
  (loop
     (handler-case
         (let ((idx (read-byte stream)))
           (if (< idx commands-length)
               (progn
                 (funcall (nth idx commands) storage stream)
                 (force-output stream))
               (return)))
       (end-of-file ()
         (return))
       (error (e)
         (syslog :err e)))))
