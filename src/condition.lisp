(in-package #:hathor)

(define-condition storage-error (condition)
  ((code :reader code :initarg :code)
   (message :reader message :initarg :message)))

(define-condition write-error (storage-error)
  ())

(define-condition read-error (storage-error)
  ())
