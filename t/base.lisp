(in-package #:hathor-test)

(def-suite base :description "Test that the basic features are working.")

(in-suite base)

(defvar *address* "127.0.0.1")
(defvar *port* 8888)
(defvar *commands*
  (list #'hathor::command-ping
        #'hathor::command-set
        #'hathor::command-get)
  "Order here is very important. It defines
how the server understands the first byte
of client requests.")

(defmacro with-server (&body body)
  (let ((server (gensym)))
    `(progn
       (let ((,server (hathor:start-server *address* *port*
                                           nil *commands*)))
         (unwind-protect
              (progn ,@body)
           (progn
             (hathor:stop-server ,server)))))))

(test server-starts
  (with-server
    (is-true t)))

(test server-ping
  (with-server
    (usocket:with-client-socket (socket stream *address* *port*
                                        :element-type 'hathor::octet)
      (write-byte 0 stream)
      (force-output stream)
      (is-true (= 0 (read-byte stream))))))

(test unknown-command
  (with-server
    (usocket:with-client-socket (socket stream *address* *port*
                                        :element-type 'hathor::octet)
      (write-byte 254 stream)
      (force-output stream)
      (let ((success nil))
        (handler-case
            (read-byte stream)
          (end-of-file ()
            (setf success t)))
        (is-true success)))))
