(asdf:defsystem #:hathor-test
  :description "Tests for hathor."
  :author "Florian Margaine <florian@margaine.com>"
  :license "GPLv3"
  :serial t
  :depends-on (:hathor :fiveam :babel)
  :components ((:module "t"
                        :components ((:file "package")
                                     (:file "base" :depends-on ("package"))))))
